# coding: ascii

import random
import time

count = 0
rand = random.Random()

def nth(a, i):
        global count
        if len(a) <= 1:
                if i == 0: return a[0]
                else: raise Error, "nth error"
        idx = rand.randint(0, len(a) - 1)
        m = a[idx]
        del a[idx]
        left, right = [], []
        for x in a:
                count += 1
                if x <= m: left.append(x)
                else: right.append(x)
        if len(right) == 0: right.append(m)
        else: left.append(m)
        if len(left) <= i: return nth(right, i - len(left))
        else: return nth(left, i)

def test():
        global count, rand
        n = 1000
        # m = [rand.uniform(0.0, 1.0) for i in range(n)]
        m = [0.5 for i in range(n)]
        m1 = m[:]
        t1 = time.time()
        m1.sort()
        t2 = time.time()
        print "%f sec" % (t2 - t1)
        idx = n / 2
        print m1[idx]
        count = 0
        t1 = time.time()
        ans = nth(m, idx)
        t2 = time.time()
        print ans
        print "%f sec" % (t2 - t1)
        print "count = %d" % count
        
test()

