# coding: ascii

import random
import time

count = 0

def middle5(a):
        global count
        n = len(a)
        if n > 5: raise Error, "middle5 error"
        if len(a) <= 2: return a[0]
        else:
                for i in range(n - 1):
                        for j in range(n - 1 - i):
                                count += 1
                                if a[j] > a[j + 1]:
                                        a[j], a[j + 1] = a[j + 1], a[j]
                return a[n / 2]

def partition5(a):
        aa, mm = a[:], []
        while True:
                if len(aa) > 5:
                        mm.append(middle5(aa[:5]))
                        aa = aa[5:]
                else:
                        mm.append(middle5(aa))
                        return mm

def nth(a, i):
        global count
        if len(a) <= 1:
                if i == 0: return a[0]
                else: raise Error, "nth error"
        elif len(a) <= 5:
                aa = a[:]
                aa.sort()
                return aa[i]
        elif max(a) == min(a):
                return a[0]
        m = nth(partition5(a), len(a) / 5 / 2)
        left, right = [], []
        for x in a:
                count += 1
                if x <= m: left.append(x)
                else: right.append(x)
        if len(left) <= i: return nth(right, i - len(left))
        else: return nth(left, i)

def test():
        global count
        r = random.Random()
        n = 100000
        m = [r.uniform(0.0, 1.0) for i in range(n)]
        m1 = m[:]
        m1.sort()
        print m1[n / 2]
        count = 0
        t1 = time.time()
        ans = nth(m, n / 2)
        t2 = time.time()
        print ans
        print "%f sec" % (t2 - t1)
        print "count = %d" % count
        

test()

