
# n = 9145356185469980673640298696124239
n = 58528822289321889575217956661469490851

def gcd(x, y):
    if x == 0:
        return y
    elif y == 0:
        return x
    else:
        return gcd(y, x % y)
def g(x):
    return (x * x + 1) % n
def gg(x):
    return g(g(x))


def work():
    x=1
    y=1
    count = 0
    count2 = 0
    pp = 1
    while True:
        x = g(x)
        y = gg(y)
        pp = (abs(x - y) * pp) % n
        if count2 % 200 == 0:
            px = gcd(pp, n)
            pp = 1
            if px > 1: break
        count += 1
        count2 += 1
        if count % 100000 == 0:
            print count
    print "ans = ", px

work()
