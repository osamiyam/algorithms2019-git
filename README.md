﻿
### 2019年度 アルゴリズム特論

2019年度の「アルゴリズム特論」の資料のホームページ


回 | 日付  |タイトル 
----|------|---------
1	| 4/8 |全体の概論 ソートアルゴリズムの比較回数の下界 
2	| 4/15 | [RAMの基本（１）](./001-RAM.pdf)
3      | 4/22  |[RAMの基本（２）と再帰呼び出しの復習](./001-RAM.pdf)
4      | 4/29  |[再帰呼び出しと帰納関数](./002-再帰的な考え方２.pdf)
5      | 5/6   |[アッカーマン関数](./002-再帰的な考え方２.pdf)
6      | 5/13   |[素数判定（１）](./003-素数判定アルゴリズム.pdf)
7      | 5/20   |[素数判定（２）](./003-素数判定アルゴリズム.pdf)
8      | 5/27   |[素因数分解](./004-prime-number.pdf)
9	|6/3	|[ソーティングネットワーク](./005-sorting-network.pdf)
10	|6/10	|[ハッシュのはなし（１）](./006-hash-function.pdf)
11     | 6/17   |[ハッシュのはなし（２）](./006-hash-function.pdf)
12	|6/24	|[ハッシュのはなし（３）](./006-hash-function.pdf)
13      | 7/1| [順序統計量](./007-順序統計量.pdf)
14      | 7/8| [並列計算について（OpenMPとCUDA）](./008-parallel.pdf)


- 一回目のレポートを出題しました（4/22）．5/20に提出してください．
- 松見君がRAM.javaの不具合を修正してくれました．ソースコードが修正されています．
- ２回目のレポートを出題しました（5/27)．6/17に提出してください．
- 第13回の授業の説明に使ったプログラムをディレクトリNTHに入れました．nth0r.pyはそのプログラムの改訂版です．元のプログラムだと，与えられた列の内容がすべて等しい場合には無限ループになってしまいます．正しくはどちらのグループも空になることがないように調整する必要があります．その様に修正したプログラムがnth0r.pyです
- 第3回レポート問題は [最後のスライド](./008-parallel.pdf) に掲載しました．

> Written with [StackEdit](https://stackedit.io/).
